#! /usr/bin/env python

import os
import csv
import sys
import subprocess
from util import parse_id

def log(msg):
    sys.stderr.write(msg + '\n')
    sys.stderr.flush()

def lines(filename):
    i = 0
    with open(filename) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def bagofwords(text):
    bag = {}
    for word in text.split():
        lword = word.lower()
        if lword in bag:
            bag[lword]=bag[lword]+1
        else: bag[lword]=1
    return bag

def intersect(bag1,bag2):
    num = 0
    for key in bag1:
        if key in bag2:
            num += min (bag1[key],bag2[key])
    return num

def bagsize(bag):
    num=0
    for key in bag:
        num += bag[key]
    return num

#info on proper script call
queryf = sys.argv[1]
t = max(min (1.0,float(sys.argv[2])),0.0)
mgraph = open(sys.argv[3],'w')

print "Input File: " + queryf
print "Threshold for bag of words : " + str(t)
print "Output File : " + sys.argv[3]

papers = {}

with open(queryf) as queryfile:
    rdr = csv.reader(queryfile, delimiter='|')
    for row in rdr:
        if len(row) < 10:
            log('Warning: unexpected length for %s, omitting' % "|".join(row))
            continue

        paper_id = parse_id(row)
        papers[paper_id] = row[0]
    
for id1 in papers:
        bag1 = bagofwords(papers[id1])
        for id2 in papers:
            if id2>id1:
                bag2 = bagofwords(papers[id2])
                common = float(intersect(bag1,bag2))
                if common/float(max(bagsize(bag1),bagsize(bag2))) >= t:
                    #print bag1
                    #print bag2
                    #print str(common)
                    mgraph.write(id1 + " " + id2 + "\n")
                    #print papers[id1] + " " + papers[id2]
mgraph.close()
                    

