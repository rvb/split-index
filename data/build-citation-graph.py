#! /usr/bin/env python2

# Build citation graph file and list containing the number of
# citations omitted during scraping for each article.

# Usage: python2 build-citation-graph.py AUTHORPATH MAXCITES Writes
# the graph file to AUTHORPATH.gra and the omitted citations to
# AUTHORPATH.omitted-citations.

import os
import csv
import sys
import subprocess
from util import parse_id


def log(msg):
    sys.stderr.write(msg + '\n')
    sys.stderr.flush()


def lines(filename):
    i = 0
    with open(filename) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def handle_article(paper_id, num_citations, max_citations, graphfile,
                   articlefile):
    log("Working on %s" % paper_id)
    datafilename = data_dir + ('/cites-%s.list' % paper_id)

    if not os.path.isfile(datafilename):
        log('Warning: %s does not exist, omitting' % datafilename)
        assert num_citations == 0 or citations > max_citations, ("No citations were fetched for %s and its citations (%i) are below the threshold (%i)" % (paper_id, num_citations, max_citations))
        articlefile.write("%s %d\n" % (paper_id, num_citations))
        return

    if not (lines(datafilename) >= num_citations):
        log('Warning: Data file for %s is incomplete, omitting' % paper_id)
        assert num_citations == 0 or num_citations > max_citations, ("Not all citations were fetched for %s." % paper_id)

    data_lines = 0
    with open(datafilename, 'rb') as df:
        rdr = csv.reader(df, delimiter='|')
        for row in rdr:
            if len(row) < 10:
                log('Warning: unexpected length for %s, omitting' % "|".join(row))
                continue
            else:
                citing_paper_id = parse_id(row)
                graphfile.write("%s %s\n" % (paper_id, citing_paper_id))
                data_lines += 1
    articlefile.write("%s %d\n" %
                      (paper_id, max(0, num_citations - data_lines)))

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print "Usage: python2 build-citation-graph.py path_to_author_data_directory max_citations"
        sys.exit()
    
    data_dir = sys.argv[1]
    max_citations = int(sys.argv[2])
    
    with open(data_dir + "/query.list", 'rb') as csvfile:
        rdr = csv.reader(csvfile, delimiter='|')

        with open(data_dir + ".gra", 'w') as graphfile:
            with open(data_dir + ".articles", 'w') as articlefile:

                for row in rdr:
                    if len(row) < 10:
                        log('Warning: unexpected length for %s, omitting' % "|".join(row))
                        continue

                    citations = int(row[3])
                    # log('%s has %i citations' % (row, citations))

                    paper_id = parse_id(row)
                    handle_article(paper_id, citations, max_citations,
                                   graphfile, articlefile)
