import sys
import math

import networkx.algorithms.approximation as app
import itertools
import networkx.classes.function as fun
import networkx.algorithms.clique as cl
import networkx as nx
import numpy as np
from itertools import chain
from collections import defaultdict
from copy import copy
import pickle
import os.path

from util import read_comp_graph

def compute_merges_halldor(G):
    return app.clique_removal(G)[1]


def protected_degree(G,x):
    print(x,G.degree(x))
    print(x,G.degree(x))
    if G.degree(x) == None :
        return 0
    else:
        return G.degree(x)


def compute_merges_greedy(G, maxdeg):
    # maxdeg = True -> start greedily from nodes of largest degree
    #        = False -> from smallest degree
    vertices = sorted(list(G.nodes()), key = lambda x: G.degree(x), reverse = maxdeg)
    done = {x: False for x in vertices}

    initial_merges = []

    for v in vertices:
        if not done[v]:
            new_clique = set([v])
            neighbors = set(filter(lambda x: not done[x], G.neighbors(v)))
            while len(neighbors) > 0:
                v = neighbors.pop()
                new_clique.add(v)
                neighbors.intersection_update(set(filter(lambda x: not done[x], G.neighbors(v))))

            for u in new_clique:
                done[u] = True
            initial_merges.append(new_clique)

    return initial_merges

def compute_merges_maximum(G):
    vertices = sorted(list(G.nodes()), key = lambda x: G.degree(x))
    not_done = set(G.nodes())

    initial_merges = []

    while len(not_done) > 0:
        max_clique = set(max(cl.find_cliques(G.subgraph(not_done)), key = len))
        initial_merges.append(max_clique)
        # print max_clique
        not_done -= max_clique

    return initial_merges

def generate_merges(merge_method, path, author, comp, G):

    assert merge_method in ["halldor", "greedy_maxdeg", "greedy_mindeg", "maximum"], "Unknown merge method " + merge_method

    # Load or generate the initial merges.
    initial_merges = None
    mergefile = path + author + comp + '.merge-' + merge_method
    if os.path.isfile(mergefile):
        # print "reading"
        with open(mergefile, 'rb') as fh:
            initial_merges = pickle.load(fh)
    else:
        # print "generating + writing"
        if merge_method == "halldor":
            initial_merges = compute_merges_halldor(G)
        elif merge_method == "greedy_maxdeg":
            initial_merges = compute_merges_greedy(G, True)
        elif merge_method == "greedy_mindeg":
            initial_merges = compute_merges_greedy(G, False)
        elif merge_method == "maximum":
            initial_merges = compute_merges_maximum(G)

        with open(mergefile, 'wb') as fh:
            pickle.dump(initial_merges, fh)

    # Sanity checks
    all_sets = True
    for i in initial_merges:
        new = isinstance(i, set)
        if not new:
            print(i)
        all_sets &= new

    assert all_sets, "At least one non-set item for " + author

    # print "Nodes " + str(G.nodes())
    # print "Merges " + str(initial_merges)
    flattened = [v for l in initial_merges for v in l]
    node_not_merge = set(G.nodes()) - set(flattened)
    merge_not_node = set(flattened) - set(G.nodes())
    assert len(node_not_merge) <= 0, str(node_not_merge) + " in nodes but not in merges" + merge_method
    assert len(merge_not_node) <= 0, str(merge_not_node) + " in merges but not in nodes" + merge_method
    # print (set(G.nodes()) - set(flattened)) | (set(flattened) - set(G.nodes()))

    return initial_merges


if __name__ == "__main__":
    # filenames
    if len(sys.argv) < 4:
        print("Usage: python2 build-initial-merges.py path_to_author_file author_name comp_threshold")
        sys.exit()
    path = sys.argv[1]
    author = sys.argv[2]
    comp = sys.argv[3]

    G = read_comp_graph(path + author, comp)

    for merge_method in ["halldor", "greedy_maxdeg", "greedy_mindeg", "maximum"]:
        initial_merges = generate_merges(merge_method, path, author, str(comp), G)
