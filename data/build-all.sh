#!/bin/sh

set -x

./build-citation-graphs.sh
./build-compatibility-graphs.sh
./build-purged-articles.sh
./build-all-initial-merges.sh
