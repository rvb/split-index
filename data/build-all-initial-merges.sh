#!/bin/sh

set -x

for dataset in `find * -type d`
do
    for i in `find $dataset/* -type d`
    do
	WHO=$(basename $i)
	for comp in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 
	do
	    python3 build-initial-merges.py $dataset/ $WHO $comp 
	done
    done
done
