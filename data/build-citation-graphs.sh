#! /usr/bin/env bash

set -x

for AUTHORPATH in `find */* -type d`; do
    
    MAXCITES=$(grep "Only articles with at most " "$AUTHORPATH.info" | cut -f 6 -d " ")
    MAXCITESB=$(grep "Only articles with less than " "$AUTHORPATH.info" | cut -f 6 -d " ")

    if [ ! -z "$MAXCITES" ]; then
	python2 build-citation-graph.py $AUTHORPATH $MAXCITES
    elif [ ! -z "$MAXCITESB" ]; then
	python2 build-citation-graph.py $AUTHORPATH $((MAXCITESB-1))
    else
	python2 build-citation-graph.py $AUTHORPATH 100000
    fi
    
done
