#! /usr/bin/env bash

set -x

for AUTHORPATH in `find */* -type d`; do
    
    MAXCITES=$(grep "Only articles with at most " "$AUTHORPATH.info" | cut -f 6 -d " ")
    MAXCITESB=$(grep "Only articles with less than " "$AUTHORPATH.info" | cut -f 6 -d " ")
    
    if [ -z "$MAXCITES" ]; then
	echo 0 > "$AUTHORPATH.purged"
    else
	./articles-with-many-citations.py "$AUTHORPATH/query.list" "$MAXCITES" > "$AUTHORPATH.purged"
    fi

    if [ ! -z "$MAXCITESB" ]; then
	./articles-with-many-citations.py "$AUTHORPATH/query.list" "$((MAXCITESB-1))" > "$AUTHORPATH.purged"
    fi

done
