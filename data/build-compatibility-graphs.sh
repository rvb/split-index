#! /usr/bin/env bash

set -x

#only the query.list files are used
#these should be in the subfolder with the author name 
#output is also written to this subfolder

for f in `find */* -type d`
do 
    for i in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 
    do
	name=$f
	echo $name $i
	python2 build-compatibility-graph.py $name/query.list $i $name/$i.comp 
    done
done
