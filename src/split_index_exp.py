'''

Split Index: Maximizing h-Indices of Google Scholar profiles by
unmerging merged articles.

Copyright (C) 2016-2019 Clemens Hoffmann, Manuel Sorge, and Kolja
Stahl

For licensing see LICENSE.

'''

import sys
import math
import itertools
import pickle
import os.path

from util import log, read_cite_graph, read_comp_graph


def valid_partition(P, G):
    occurrence = {v: 0 for v in G.nodes()}
    for p in P:
        for v in p:
            occurrence[v] += 1
    return all(occurrence[v] == 1 for v in G.nodes())


def num_citations(v, D):
    """Compute the citations of an article.
    """
    return D.in_degree(v) + D.node[v]['purged']


def sum_cite(p, D):
    """Add the citations of all elements in p
    """
    all_citations = 0
    for v in p:
        assert(v in D.nodes()), v
        all_citations += num_citations(v, D)
    return all_citations


# Union Cite: Multiple references to elements in a partition count as one
def union_cite(p, D):
    """Take union of citations of elements of p
    """
    graph_citations = set()
    purged_citations = 0
    for v in p:
        assert v in D.nodes(), str(v) + " not in citation graph"
        graph_citations.update(D.predecessors(v))
        purged_citations += D.node[v]['purged']
    return len(graph_citations) + purged_citations


# Fusion Cite: References of elements within a partition are discarded
def fusion_cite(p, D, P):
    """Take union of citations of p but remove self-citations of p, count
for each p' in P only one citation.

    """
    graph_citations = set()
    purged_citations = 0
    # Find all citations of p.
    for v in p:
        graph_citations.update(D.predecessors(v))
        purged_citations += D.node[v]['purged']

    # Find excess citations from own articles
    excess = 0
    for p_ in P:
        this_part_cites = [v for v in p_ if v in graph_citations]
        # Since P is a partition of a subset of all_citations, we can
        # safely subtract len(this_part_cites) - 1 from
        # len(all_citations) to get at most one citation from each
        # merged article in P.
        if len(this_part_cites) == 0:
            continue
        if p_ == p:
            excess += len(this_part_cites)
        else:
            excess += len(this_part_cites) - 1

    return len(graph_citations) + purged_citations - excess


def nparts_with_many_citations(partition, cite_graph, measure, how_many):
    """Compute number of parts with at least how_many citations.

    """
    ret = None
    if measure == fusion_cite:
        ret = len([ncites for ncites in
                   [fusion_cite(merged_article, D, partition)
                    for merged_article in partition]
                   if ncites >= how_many])
    else:
        ret = len([ncites for ncites in
                   [measure(merged_article, D) for merged_article in partition]
                   if ncites >= how_many])

    return ret


def h_index(partition, cite_graph, measure):
    """Compute h_index of partititon.

    """
    profile = None
    if measure == fusion_cite:
        profile = sorted([measure(x, cite_graph, partition) for x in
                          partition[:]], reverse=True)
    else:
        profile = sorted([measure(x, cite_graph) for x in
                          partition[:]], reverse=True)

    i = 0
    while i < profile[i]:
        i += 1

    return i


def conservative_atomizing(P, D, h, k, mu):
    """Atomize at most k parts of P so as to maximize the number of parts
    with at least h citations.

    """
    P = [list(x) for x in P]
    # makes sure that each entry of P is a list, i.e. list([1, 2]) = [1, 2]

    R = P[:]  # This performs a shallow copy

    counts = []
    for p in P:
        countp = 0
        for A in p:
            if mu([A], D) >= h:
                countp += 1
        if mu(p, D) >= h:
            countp -= 1

        counts.append((countp, p))

    best = sorted(counts, reverse=True, key=lambda x: x[0])[:k]

    for (countp, pprime) in best:
        if countp > 0:
            R.remove(pprime)
            list(map(lambda x: R.append([x]), pprime))
    return R


def fusion_conservative_atomizing(P, D, h, k, _):
    """Greedily atomize k times one of the parts of P that when
individually atomized maximize the increase of parts with at least h
citations according to fusion_cite.

    """

    P_copy = []
    for index, val in enumerate(P):
        P_copy.append(list(P[index]))
    P = P_copy

    for _ in itertools.repeat(None, k):
        cache = []
        # Another deep copy we're working on, as we can't iterate and
        # delete from a list at the same time
        R = []
        for index, val in enumerate(P):
            R.append(list(P[index]))

        current_hcited = nparts_with_many_citations(P, D, fusion_cite, h)

        for index, p in enumerate(P):

            if len(P[index]) > 1:
                # Find how many new articles with at least h
                # citations we get by atomizing p.

                for index2, a in enumerate(p):
                    R.append([a])
                del R[index]

                new_hcited = nparts_with_many_citations(R, D, fusion_cite, h)

                cache.append((index, new_hcited))

                for index2, a in enumerate(p):
                    R.remove([a])
                R.insert(index, p)  # Does not get copied.

        if len(cache) == 0:
            break

        index, new_hcited = sorted(cache, reverse=True, key=lambda x: x[1])[0]
        if new_hcited <= current_hcited:
            break

        for part in P[index]:  # add the new subsets, as computed before
            P.append([part])
        del P[index]            # remove the old subset

    return P


def cautious_extracting(P, D, h, k, mu):
    """Extract at most k articles out of parts of P to maximize the number
of parts with at least h citations.

    """
    R = []
    for p in P:                 # For all merged articles
        p_ = list(p)[:]         # get a copy of its atomic articles.
        for v in p:             # For each atomic article
            if k <= 0:
                break

            p_.remove(v)
            if mu([v], D) >= h and mu(p_, D) >= h:
                # if removing the article yields two h-index articles
                R.append([v])   # do the removal.
                k -= 1
            else:
                p_.append(v)

        if len(p_) > 0:
            R.append(p_)
    return R


def fusion_cautious_extracting(P, D, h, k, _):
    """Greedily extract at most k articles out of parts of P that
individually maximize the increase in the number of parts with at
least h citations.

    """
    # Another deep copy we're working on, as we can't iterate and delete from
    # a list at the same time
    if k == 0:
        return P

    R = []
    for index, val in enumerate(P):
        R.append(list(P[index]))
    for index, p in enumerate(P):  # For each merged article p
        if len(R[index]) > 1:

            for index2, a in enumerate(p):  # For each atomic article a in p
                if len(R[index]) > 1:
                    # Try to remove a and see if it increases the h_index
                    R.append([a])
                    R[index].remove(a)
                    if fusion_cite([a], D, R) >= h and fusion_cite(R[index],
                                                                   D, R) >= h:
                        k -= 1
                        if k == 0:
                            return R
                    else:
                        R.remove([a])
                        R[index].append(a)

    return R


def conservative_extracting(P, D, h, k, mu):
    """Remove individual articles out of at most k parts of P to maximize
the number of parts that have at least h citations.

    """
    k_elems = []
    for p in P:                 # For each merged article p
        lp = 0                  # The max number of h-citation
                                # articles we can get out of p.
        Rp = []                 # The corresponding set of articles.
        p_ = list(p)[:]
        for v in p:
            p_.remove(v)
            if mu([v], D) >= h and mu(p_, D) >= h:
                Rp.append([v])
                lp += 1
            else:
                p_.append(v)
        if len(p_) > 0:
            Rp.append(p_)

        k_elems.append((lp, Rp, p))

    # Find those merged articles from which we get the most h-citation
    # articles.
    best = sorted(k_elems, reverse=True, key=lambda x: x[0])[:k]
    R = []
    pprime = P[:]
    for (_, rp, p) in best:
        pprime.remove(p)
        R.extend(rp)
    R.extend([list(x) for x in pprime])
    return R


def fusion_conservative_extracting(P, D, h, k, _):
    """Do k times the following: Find a part p in P from which the largest
number of articles can be iteratively extracted while increasing the
number of articles with at least h citations. Extract all of these
articles from p.

    """

    # Work on a deep copy of P in order to avoid side effects
    P_copy = []
    for index, val in enumerate(P):
        P_copy.append(list(P[index]))
    P = P_copy

    for _ in itertools.repeat(None, k):
        cache = []
        # Another deep copy we're working on, as we can't iterate and
        # delete from a list at the same time
        R = []
        for index, val in enumerate(P):
            R.append(list(P[index]))
        for index, p in enumerate(P):
            if len(R[index]) > 1:
                lp = 0
                new_partition = []
                for index2, a in enumerate(p):
                    if len(R[index]) > 1:
                        R.append([a])
                        # a in R[index]
                        R[index].remove(a)
                        if fusion_cite([a], D, R) >= h and \
                           fusion_cite(R[index], D, R) >= h:
                            lp += 1
                            new_partition.append([a])
                        else:
                            R.remove([a])
                            R[index].append(a)
                if len(R[index]) > 0:
                    new_partition.append(R[index])
                cache.append((index, new_partition, lp))

        if len(cache) > 0:
            index, new_partition, _ = sorted(cache, reverse=True,
                                             key=lambda x: x[2])[0]
            del P[index]  # remove the old subset
            for part in new_partition:
                # add the new subsets, as computed before
                P.append(part)
    return P


def read_graphs(path, dataset, author, comp):
    filedir = path + dataset + '/'
    G = read_comp_graph(filedir + author, comp)

    D = read_cite_graph(filedir + author)

    # Sanity checks
    Gn = G.nodes()
    Dn = D.nodes()

    # Check that each article of the author has some well-defined
    # citation value, i.e. occurs in the citation graph.
    merge_not_cite = set(Gn) - set(Dn)
    assert len(merge_not_cite) <= 0, str(merge_not_cite) + " in compatibility graph but not in citation graph. author:" + dataset + author

    for a in Dn:
        assert not (D.in_degree(a) > 0 and (a not in Gn)), ("%s in citation graph (with > 0 citations) but not in compatibility graph for %s" % (a, author))

    # return compatibility graph G, citation graph D
    return G, D


def read_merges(merge_method, path, dataset, author, comp, G):
    filedir = path + dataset + '/'

    # Load the initial merges.
    initial_merges = None
    mergefile = filedir + author + comp + '.merge-' + merge_method
    if os.path.isfile(mergefile):
        # print "reading"
        with open(mergefile, 'rb') as fh:
            initial_merges = pickle.load(fh)
    else:
        raise RuntimeError("Initial merges not found.")

    # Sanity checks
    all_sets = True
    for i in initial_merges:
        new = isinstance(i, set)
        if not new:
            print(i)
        all_sets &= new

    assert all_sets, "At least one non-set item for " + author

    # print "Nodes " + str(G.nodes())
    # print "Merges " + str(initial_merges)
    flattened = [v for l in initial_merges for v in l]
    node_not_merge = set(G.nodes()) - set(flattened)
    merge_not_node = set(flattened) - set(G.nodes())
    assert len(node_not_merge) <= 0, \
        str(node_not_merge) + " in nodes but not in merges" + merge_method
    assert len(merge_not_node) <= 0, \
        str(merge_not_node) + " in merges but not in nodes" + merge_method

    return initial_merges


def hindex_upper_bound(D, merges, hindex_lower_bound):
    """Compute upper bounds on the hindex that can be achieved by finer
partitions than merges and if the hindex is at least
hindex_lower_bound.

    """
    edge_bound = int(math.floor(math.sqrt(D.number_of_edges())))
    # print(edge_bound)

    max_new_articles = 0
    for p in merges:
        potential = [v for v in p if num_citations(v, D) >= hindex_lower_bound]
        max_new_articles += max(0, len(potential) - 1)
    new_article_bound = hindex_lower_bound + max_new_articles
    # print(new_article_bound)

    return min(edge_bound, new_article_bound)


def highest_hindex(D, merge_method, initial_merges, dataset, author,
                   comp, f, k, measure):
    """Compute the largest h_index achievable in problem f with measure
and at most k operations.

    """

    # Compute h-index respecting the initial merges
    hmerged = h_index(initial_merges, D, measure)

    # Successively check whether we can get at least h articles with h
    # citations for increasing h.

    # Upper bound on achievable h-index
    hindex_ub = hindex_upper_bound(D, initial_merges, hmerged)

    max_hindex = hmerged  # Largest so-far achieved h-index
    for h in range(hmerged + 1, hindex_ub + 1):
        new_merges = f(initial_merges, D, h, k, measure)

        assert valid_partition(new_merges, G), \
            "No valid partition for %s, %s, %s, %s, %s: %s" % \
            (dataset, author, comp, f.__name__, measure.__name__, new_merges)
        new_parts = nparts_with_many_citations(new_merges, D, measure, h)

        if new_parts >= h:
            max_hindex = h

        if measure != fusion_cite and new_parts < h:
            assert not max_hindex == 0, \
                "H-index after first modification smaller than initial h-index"
            # h -= 1
            break

    if k == 0:
        assert max_hindex <= hmerged, \
            "could improve without operations for %s, %s, %s, %s, %s" % \
            (dataset, author, comp, f.__name__, measure.__name__)

    print(dataset, author, comp, merge_method, measure.__name__,
          f.__name__, hmerged, max_hindex, hindex_ub, k)


def max_conservative_operations(initial_merges):
    large_cliques = [x for x in initial_merges if len(x) > 1]
    return len(large_cliques)


def max_cautious_operations(initial_merges):
    large_cliques = [x for x in initial_merges if len(x) > 1]
    sumlen = 0
    for clique in large_cliques:
        sumlen += len(clique) - 1
    return sumlen


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("""
Usage: python3 split-index-exp.py path_to_data

[path_to_data] is expected to contain a set of directories (datasets).
Each dataset is expected to contain a set of directories (authors).
Each dataset is also expected to contain, for each author,
- [author].articles - the list of articles of [author],
- [author].gra - the citation graph
- [author]/[t].comp - the compatibility graph for each t in {0.1, 0.2,
  ..., 0.9}
- [author].merge[t]-[m] - the initially merged articles for each t in
  {0.1, 0.2, ... 0.9} and each m in {halldor, greedy_maxdeg,
  greedy_mindeg, maximum}
        """)
        sys.exit()

    path = sys.argv[1]
    if not os.path.isdir(path):
        raise RuntimeError("Target path is not a directory.")

    if path[-1] != '/':
        path += '/'

    for dataset in [name for name in os.listdir(path)
                    if os.path.isdir(path + name)]:
        for author in [name for name in os.listdir(path + dataset)
                       if os.path.isdir(path + dataset + '/' + name)]:
            for comp in [str(x / 10) for x in range(1, 10)]:

                G, D = read_graphs(path, dataset, author, comp)
                for merge_method in ["halldor", "greedy_maxdeg",
                                     "greedy_mindeg", "maximum"]:
                    initial_merges = read_merges(merge_method, path,
                                                 dataset, author, comp, G)

                    for f in {conservative_atomizing,
                              cautious_extracting, conservative_extracting}:

                        log("Working on dataset %s / author %s / comp %s /"
                            "merge method %s / problem %s" %
                            (dataset, author, comp, merge_method, f.__name__))

                        if f.__name__.startswith("conservative"):
                            max_k = max_conservative_operations(initial_merges)
                        else:
                            max_k = max_cautious_operations(initial_merges)

                        for k in range(1, max_k + 1, 1):
                            highest_hindex(D, merge_method,
                                           initial_merges, dataset,
                                           author, comp, f, k,
                                           sum_cite)
                            highest_hindex(D, merge_method,
                                           initial_merges, dataset,
                                           author, comp, f, k,
                                           union_cite)

                    for f in {fusion_conservative_atomizing,
                              fusion_cautious_extracting,
                              fusion_conservative_extracting}:

                        log("Working on dataset %s / author %s / comp %s /"
                            "merge method %s / problem %s" %
                            (dataset, author, comp, merge_method, f.__name__))

                        if f.__name__.startswith("fusion_conservative"):
                            max_k = max_conservative_operations(initial_merges)
                        else:
                            max_k = max_cautious_operations(initial_merges)

                        for k in range(1, max_k + 1, 1):
                            highest_hindex(D, merge_method,
                                           initial_merges, dataset,
                                           author, comp, f, k,
                                           fusion_cite)
